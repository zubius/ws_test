﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Text;

public class EchoTest : MonoBehaviour {

	// Use this for initialization
//	WebSocket w = new WebSocket(new Uri("ws://echo.websocket.org"));
//	WebSocket w = new WebSocket(new Uri("ws://192.168.88.86:8123/"));
	WebSocket w = new WebSocket(new Uri("ws://localhost:8123/"));
	public Text text;

	public Text input;

	void Start () {

	}

	public void SendText() {
		if (input.text.Length > 0)
			w.SendString(input.text);
	}

	void OnApplicationQuit() {
		w.Close ();
	}

	public void Connect() {
		StartCoroutine(_connect());
	}

	public void Disconect() {
		w.Close();
	}

	bool isError = false;
	string reply = "";
	IEnumerator _connect() {
		yield return StartCoroutine(w.Connect());
//		w.SendString("Hi there");
		isError = true;
		int i=0;
		while (true)
		{
			string reply = w.RecvString();
			if (reply != null)
			{
				Debug.Log ("<--- "+reply);
				text.text = reply;
//				w.SendString("Hi there"+i++);
				isError = true;
			} else if (isError) {
				isError = false;
				Debug.Log(reply);
			}
			if (w.Error != null)
			{
				Debug.LogError ("Error: "+w.Error);
				break;
			}
			yield return 0;
		}
		w.Close();
	}
}
